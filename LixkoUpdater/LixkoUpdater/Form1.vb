﻿Imports System.Net
Public Class Main

    Private Sub CheckButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckButton.Click
        Dim client As WebClient = New WebClient
        AddHandler client.DownloadProgressChanged, AddressOf client_ProgressChanged
        AddHandler client.DownloadFileCompleted, AddressOf client_DownloadCompleted
        If My.Computer.FileSystem.FileExists("C:\Users\Erik\Desktop\LixkoUpdater\check") Then
            Try
                My.Computer.FileSystem.DeleteFile("C:\Users\Erik\Desktop\LixkoUpdater\check")
                StatusBarProgress1.Visible = True
                StatusBarStepLabel.Text = "Downloading..."
            Catch ex As Exception
                StatusBarStepLabel.Text = ex.Message
            End Try
        End If

        client.DownloadFileAsync(New Uri("http://localhost/lixko/check.php"), "C:\Users\Erik\Desktop\LixkoUpdater\check")
        CheckButton.Text = "Checking..."
        CheckButton.Enabled = False
    '    Dim WC As New WebClient
    '   WC.DownloadFileAsync(New Uri("http://localhost/check.php"), "checkfile-" + TimeOfDay.Date)
    End Sub

    Private Sub client_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
        Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
        Dim percentage As Double = bytesIn / totalBytes * 100

        StatusBarProgress1.Value = Int32.Parse(Math.Truncate(percentage).ToString())
    End Sub

    Private Sub client_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        'MessageBox.Show("Download Complete")
        CheckButton.Text = "Check!"
        CheckButton.Enabled = True
        Label1.Visible = True
        Label2.Visible = True
        Label2.Text = My.Computer.FileSystem.ReadAllText("C:\Users\Erik\Desktop\LixkoUpdater\check")
        StatusBarProgress1.Visible = False
        StatusBarStepLabel.Text = "Idle"
        log("File downloaded!", "i")
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        TabControl1.Width = Me.Width - 14
        TabControl1.Height = Me.Height - 57
    End Sub

    Private Sub log(ByVal text As String, ByVal kind As String)
        Dim kindformat As String = ""
        If kind.ToLower.Contains("debug") Or kind.ToLower.Equals("d") Then
            kindformat = "[DEBUG]"
        ElseIf kind.ToLower.Contains("info") Or kind.ToLower.Equals("i") Then
            kindformat = "[INFO]"
        ElseIf kind.ToLower.Contains("warning") Or kind.ToLower.Equals("w") Then
            kindformat = "[WARNING]"
        ElseIf kind.ToLower.Contains("error") Or kind.ToLower.Equals("e") Then
            kindformat = "[ERROR]"
        ElseIf kind.ToLower.Contains("fatal") Or kind.ToLower.Equals("f") Then
            kindformat = "[FATAL]"
        End If
        LogBox1.AppendText(kindformat + " [" + DateAndTime.Today + " " + DateAndTime.TimeString + "] " + text + Environment.NewLine)
    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log(Application.StartupPath, "d")
        log(Application.CommonAppDataPath, "d")
    End Sub
End Class
